/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.
I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#ifndef SCHEDULE_H
#define SCHEDULE_H
#include "Item.h"
#include "Order.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>

using namespace std;

class Schedule
{
public:
	//Default constructor for reading from file
	Schedule(){};

	//Constructor for when a random order has been generated
	Schedule(int mode)
	{
		//Getting an order directly from the generator
		if (mode == 0)
		{
			setOrderText(generateBundle());
			
		}
		//Getting an order from the random bundle.txt file
		else if (mode == 1)
		{
			generateBundle();
			//Read the random bundle from its output file and create a schedule from it
			ifstream filein;
			filein.open("bundle.txt");
			if (filein.fail()) {
				cout << "Input file opening failed.\n";
				exit(1);
			}
			stringstream buffer;
			buffer << filein.rdbuf();
			setOrderText(buffer.str());
		}
	}

	//Generates a random bundle of orders
    string generateBundle()
    {
    	string generatedOrder;

    	//Setup for random number generators
		random_device rd;
		default_random_engine generator(rd());
		uniform_int_distribution<int> randQuantity(150,250);
		uniform_int_distribution<int> randPCId(0,15);
		uniform_int_distribution<int> randOrderQuantity(1,20);
		uniform_int_distribution<int> randDate(0,4);
		uniform_int_distribution<int> randProfit(15,75);

		//Randomly decides how many orders there will be
		int count = randQuantity(generator);

		//Generates random orders
		for (int i = 0; i < count; ++i)
		{
			generatedOrder.append("[");

			//orderId is just which order in the loop it's up to
			generatedOrder.append(to_string(i));
			generatedOrder.append(",");

			//Generate a random pc id
			int pcId = randPCId(generator);

			//Append to order
			generatedOrder.append(to_string(pcId));
			generatedOrder.append(",");

			//Generate and append a random quantity
			generatedOrder.append(to_string(randOrderQuantity(generator)));
			generatedOrder.append(",");

			//Generate and append a random date due
			generatedOrder.append(to_string(randDate(generator)));
			generatedOrder.append(",");

			//Creates a temporary product for getting a random profit value
			Product tempProduct(pcId);

			//Generates a random profit margin from the temporary product
			double profit = tempProduct.getCost() * (double)(randProfit(generator) /100.0);

			//Appends it to the order
			generatedOrder.append(to_string((int)profit));
			generatedOrder.append("]");
		}

		//Writes the order to a file for later (not used in the program)
		ofstream fileout;
		fileout.open("bundle.txt");
		if (fileout.fail()) {
			cout << "Output file opening failed.\n";
			exit(1);
		}
		fileout << generatedOrder;

		//Returns a string of the generated order
		return generatedOrder;
    }

	//Sets the order text of the schedule
	void setOrderText(string s)
	{
		orderText = s;
	}

	//Sorts orders by their totalProfit, for easy choosing of profitable orders
	struct profitSort
	{
		inline bool operator() (const Order& order1, const Order& order2)
		{
			return (order1.getTotalProfit() > order2.getTotalProfit());
		}	
	};
	

	//Reads the order bundle from the default file
    void createOrderVectorFromFile()
    {
		//No random order, read from default file
		ifstream filein;
		filein.open("orderbundle.txt");
		if (filein.fail()) {
			cout << "Input file opening failed.\n";
			exit(1);
		}
		
		/*Gets each item from the order, places it in a temporary order,
		  then places that order in the vector*/
		while (!filein.eof())
		{
			char delimiter;
			int orderId, PCId, quantity, dueDate, profit;
			filein.get(delimiter);
			filein >> orderId;
			filein.get(delimiter);
			filein >> PCId;
			filein.get(delimiter);
			filein >> quantity;
			filein.get(delimiter);
			filein >> dueDate;
			filein.get(delimiter);
			filein >> profit;
			filein.get(delimiter);
			Order tempOrder(orderId,PCId,quantity,dueDate,profit);
			if (filein.eof())
			{
				break;
			}
			orderVector.push_back(tempOrder);
		}

		//Sorts the order vector according to their total profit
		sort(orderVector.begin(), orderVector.end(), profitSort());
	}

    void createOrderVectorFromString()
	{
		/*Random order, read from orderText
		  (either direct from the program, or from bundle.txt)*/
		istringstream stringin (orderText);

		/*Gets each item from the order, places it in a temporary order,
		  then places that order in the vector*/
		while((stringin.rdbuf()->in_avail()) > 0)
		{
			char delimiter;
			int orderId, PCId, quantity, dueDate, profit;
			stringin.get(delimiter);
			stringin >> orderId;
			stringin.get(delimiter);
			stringin >> PCId;
			stringin.get(delimiter);
			stringin >> quantity;
			stringin.get(delimiter);
			stringin >> dueDate;
			stringin.get(delimiter);
			stringin >> profit;
			stringin.get(delimiter);
			Order tempOrder(orderId,PCId,quantity,dueDate,profit);
			if (stringin.eof())
			{
				break;
			}
			orderVector.push_back(tempOrder);
		}

		//Sorts the order vector according to their total profit
		sort(orderVector.begin(), orderVector.end(), profitSort());
	}

	//Generates and outputs the schedule
    void outputSchedule()
    {
    	//Opens schedule file for writing
		ofstream fileout;
		fileout.open("scheduleoutput.txt");
		if (fileout.fail()) {
			cout << "Output file opening failed.\n";
			exit(1);
		}

		//Counts the number of cycles currently used
		int cycleCount = 0;

		//Counts the number of orders
		int noOrders = 0;

		fileout << "Satisfied orders: ";

		/*Loops through all the items in the vector to figure out
		  how many orders can be satisfied*/
		for (int i = 0; i < orderVector.size(); ++i)
		{
			//Adds the number of cycles needed for the order to the cycleCount
			cycleCount += orderVector[i].getOrderCycles();

			/*If we go over the cycle limit record the number
			  of orders satisfied and break out*/
			if (cycleCount > 10000)
			{
				//Print out the number of satisfied orders
				noOrders = i;
				fileout << noOrders << endl;
				break;
			}
		}

		/*If we made it through all the orders without going over
		  the cycle limit set the number of orders to the vector size*/
		if (noOrders == 0)
		{
			//Print out the number of satisfied orders
			noOrders = orderVector.size();
			fileout << noOrders << endl;
		}

		//Print out the headings
		fileout << left << setw(10) << "OrderId" <<
				   setw(10) << "pcId" <<
				   setw(10) << "qty" <<
				   setw(10) << "dueDate" <<
				   setw(10) << "profit" <<
				   setw(10) << "day0" <<
				   setw(10) << "day1" <<
				   setw(10) << "day2" <<
				   setw(10) << "day3" <<
				   setw(10) << "day4" <<
				   setw(10) << "TtlProfit" << endl;

		//Go through all the satisfied orders and print out their details
		for (int i = 0; i < noOrders; ++i)
		{
			//Prints out basic details
			fileout << left << setw(10) << orderVector[i].getOrderId() << 
			setw(10) << orderVector[i].getPCId() << 
			setw(10) << orderVector[i].getQuantity() << 
			setw(10) << orderVector[i].getDueDate() << 
			setw(10) << orderVector[i].getProfit();

			/*Prints out the day the order will be produced,
			  then the total profit for the order*/
			switch(orderVector[i].getDueDate())
			{
				case 0:
					fileout << left << setw(10) << orderVector[i].getQuantity() <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getTotalProfit() << endl;
					break;
				case 1:
					fileout << left << setw(10) << 0 <<
					setw(10) << orderVector[i].getQuantity() <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getTotalProfit() << endl;
					break;
				case 2:
					fileout << left << setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getQuantity() <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getTotalProfit() << endl;
					break;
				case 3:
					fileout << left << setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getQuantity() <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getTotalProfit() << endl;
					break;
				case 4:
					fileout << left << setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << 0 <<
					setw(10) << orderVector[i].getQuantity() <<
					setw(10) << orderVector[i].getTotalProfit() << endl;
					break;
			}
		}

		//Records profit before penalties
		int weekProfit = 0;
		for (int i = 0; i < noOrders; ++i)
		{
			weekProfit += orderVector[i].getTotalProfit();
		}

		//Print out headings for unsatisfied orders
		fileout << "\nUnsatisfied orders: " << orderVector.size() - noOrders << endl;
		fileout << left << setw(10) << "OrderId" <<
				   setw(10) << "pcId" <<
				   setw(10) << "qty" <<
				   setw(10) << "dueDate" <<
				   setw(10) << "profit" <<
				   setw(10) << "day0" <<
				   setw(10) << "day1" <<
				   setw(10) << "day2" <<
				   setw(10) << "day3" <<
				   setw(10) << "day4" <<
				   setw(10) << "TtlProfit" << endl;

		//Print out details for unsatisfied orders
		for (int i = noOrders; i < orderVector.size(); ++i)
		{
			orderVector[i].setTotalPenalty();
			fileout << left << setw(10) << orderVector[i].getOrderId() << 
			setw(10) << orderVector[i].getPCId() << 
			setw(10) << orderVector[i].getQuantity() << 
			setw(10) << orderVector[i].getDueDate() << 
			setw(10) << orderVector[i].getProfit() <<
			setw(10) << 0 <<
			setw(10) << 0 <<
			setw(10) << 0 <<
			setw(10) << 0 <<
			setw(10) << 0 <<
			setw(10) << orderVector[i].getTotalPenalty() << endl;
			weekProfit += orderVector[i].getTotalPenalty();
		}

		//Prints out the total taking into account cancellation penalties
		fileout << left << "\nWeek's total profit: $" <<
		weekProfit << endl << endl;

		//Close the orders file
		fileout.close();

		//Open the component demand file for writing
		fileout.open("componentdemand.txt");
		if (fileout.fail()) {
			cout << "Output file opening failed.\n";
			exit(1);
		}

		//Print out the headings for the component demand file
		fileout << "Component demand:" << endl;
		fileout << left << setw(10) << "compId" <<
				   setw(10) << "day0" <<
				   setw(10) << "day1" <<
				   setw(10) << "day2" <<
				   setw(10) << "day3" <<
				   setw(10) << "day4" <<
				   setw(10) << "total" <<endl;
		
		//Array for holding the counts of the different components
		int compCountTotal [10][2] = {{0,0}, {1,0}, {2,0}, {3,0}, {4,0},
								      {5,0}, {6,0}, {7,0}, {8,0}, {9,0}};
		int compCountDay0 [10][2] =  {{0,0}, {1,0}, {2,0}, {3,0}, {4,0},
								      {5,0}, {6,0}, {7,0}, {8,0}, {9,0}};
		int compCountDay1 [10][2] =  {{0,0}, {1,0}, {2,0}, {3,0}, {4,0},
								      {5,0}, {6,0}, {7,0}, {8,0}, {9,0}};
		int compCountDay2 [10][2] =  {{0,0}, {1,0}, {2,0}, {3,0}, {4,0},
								      {5,0}, {6,0}, {7,0}, {8,0}, {9,0}};
		int compCountDay3 [10][2] =  {{0,0}, {1,0}, {2,0}, {3,0}, {4,0},
								      {5,0}, {6,0}, {7,0}, {8,0}, {9,0}};
		int compCountDay4 [10][2] =  {{0,0}, {1,0}, {2,0}, {3,0}, {4,0},
								      {5,0}, {6,0}, {7,0}, {8,0}, {9,0}};

		//Loops through the satisfied orders
		for (int i = 0; i < noOrders; ++i)
		{
			//Get the components used in an order
			array<int,4>compArray = orderVector[i].getOrderComponents();
			for (int j = 0; j < 4; ++j)
			{
				/*For each component check which one was used,
				  then multiply it by the order quantity,
				  then add that number to the matching element in the compCountTotal.*/
				switch(compArray[j])
				{
					case 0:
						compCountTotal[0][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[0][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[0][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[0][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[0][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[0][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 1:
						compCountTotal[1][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[1][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[1][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[1][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[1][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[1][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 2:
						compCountTotal[2][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[2][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[2][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[2][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[2][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[2][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 3:
						compCountTotal[3][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[3][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[3][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[3][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[3][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[3][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 4:
						compCountTotal[4][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[4][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[4][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[4][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[4][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[4][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 5:
						compCountTotal[5][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[5][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[5][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[5][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[5][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[5][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 6:
						compCountTotal[6][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[6][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[6][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[6][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[6][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[6][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 7:
						compCountTotal[7][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[7][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[7][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[7][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[7][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[7][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 8:
						compCountTotal[8][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[8][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[8][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[8][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[8][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[8][1] += orderVector[i].getQuantity();
								break;
						}
						break;
					case 9:
						compCountTotal[9][1] += orderVector[i].getQuantity();
						/*After this added it to the compCountArray
						  for the corresponding day.*/
						switch(orderVector[i].getDueDate())
						{
							case 0:
								compCountDay0[9][1] += orderVector[i].getQuantity();
								break;
							case 1:
								compCountDay1[9][1] += orderVector[i].getQuantity();
								break;
							case 2:
								compCountDay2[9][1] += orderVector[i].getQuantity();
								break;
							case 3:
								compCountDay3[9][1] += orderVector[i].getQuantity();
								break;
							case 4:
								compCountDay4[9][1] += orderVector[i].getQuantity();
								break;
						}
						break;
				}
			}
		}
		//Print out the number of components used
		for (int i = 0; i < 10; ++i)
		{
			fileout << left << setw(10) << i << 
							   setw(10) << compCountDay0[i][1] << 
							   setw(10) << compCountDay1[i][1] << 
							   setw(10) << compCountDay2[i][1] << 
							   setw(10) << compCountDay3[i][1] << 
							   setw(10) << compCountDay4[i][1] << 
							   setw(10) << compCountTotal[i][1] << endl;
		}
    }

private:
	string orderText;
	vector <Order> orderVector;
};

#endif
/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.
I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#ifndef ORDER_H
#define ORDER_H
#include <iostream>
#include <array>
#include "Item.h"

using namespace std;

class Order
{
public:
    Order(int o, int p, int q, int d, int pr)
    {
        orderId = o; 
        PCId = p; 
        quantity = q; 
        dueDate = d; 
        profit = pr; 
        setCycles(); 
        setOrderComponents();
        setTotalProfit();
    }

    int getOrderId() const
    {
        return orderId;
    }

    int getPCId() const
    {
        return PCId;
    }

    int getQuantity() const
    {
        return quantity;
    }

    int getDueDate() const
    {
        return dueDate;
    }

    int getProfit() const
    {
        return profit;
    }

    int getOrderCycles() const
    {
        return orderCycles;
    }

    int getTotalProfit() const
    {
        return totalProfit;
    }

    int getTotalPenalty() const
    {
        return totalPenalty;
    }

    array<int,4>getOrderComponents() const
    {
        return orderComponents;
    }

    /*orderCycles is equal to the number of cycles used for a product,
      multiplied by the number of products in the order*/
    void setCycles()
    {
        Product tempProduct(PCId);
        orderCycles = quantity * tempProduct.getCycles();
    }

    //Sets the total profit for the order
    void setTotalProfit()
    {
        totalProfit = profit * quantity - orderCycles * 100;
    }

    /*Penalty of a cancelled order is the cost of production times the
      quantity of the order dividied by 2*/
    void setTotalPenalty()
    {
        Product tempProduct(PCId);
        totalPenalty = 0 - tempProduct.getCost() * quantity / 2;
    }

    //Pulls the components array from a product of id "PCId"
    void setOrderComponents()
    {
        Product tempProduct(PCId);
        orderComponents = tempProduct.getComponents();
    }
private:
    int orderId, PCId, quantity, dueDate, profit, orderCycles, totalProfit, totalPenalty;
    array<int, 4> orderComponents;
};
#endif
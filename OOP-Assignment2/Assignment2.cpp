/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.
I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

/**Compiled under g++ with the -std=c++11 standard
 * eg "g++ -std=c++11 Assignment2.cpp -o Assignment2"
 */

#include "Item.h"
#include "Component.h"
#include "Product.h"
#include "Order.h"
#include "Schedule.h"

using namespace std;

int main(int argc, char const *argv[])
{
	//Checks if one argument was supplied
	if (argc == 2)
	{
		//argument for a random order
		if (string(argv[1]) == "-r")
		{	
			//Creates a schedule + reports from the random bundle
			Schedule schedule(0);
			schedule.createOrderVectorFromString();
			schedule.outputSchedule();
		}
		//argument for default file order
		else if (string(argv[1]) == "-f")
		{
			//Creates a schedule + reports from the default bundle
			Schedule schedule;
			schedule.createOrderVectorFromFile();
			schedule.outputSchedule();
		}
		//Output documentation
		else
		{
			cout << "Usage: ./Assignment2 [OPTION] [OPTION]\n\n";
			cout << "-f                  read from default file \"orderbundle.txt\"\n";
			cout << "-r                  read random order from Bundle object\n";
			cout << "-f -r (or -r -f)    to read from the random file instead of directly from Bundle object\n";
		}
	}
	//Checks if two arguments were supplied
	else if (argc == 3)
	{
		//Were these arguments -r and -f?
		if ((string(argv[1]) == "-r" && string(argv[2]) == "-f") || (string(argv[1]) == "-f" && string(argv[2]) == "-r"))
		{
			/*Create a random bundle in "bundle.txt" and
			  create a schedule + reports from it*/
			Schedule schedule(1);
			schedule.createOrderVectorFromString();
			schedule.outputSchedule();
		}
		//If not, output documentation
		else
		{
			cout << "Usage: ./Assignment2 [OPTION] [OPTION]\n\n";
			cout << "-f                  read from default file \"orderbundle.txt\"\n";
			cout << "-r                  read random order from Bundle object\n";
			cout << "-f -r (or -r -f)    to read from the random file instead of directly from Bundle object\n";
		}
	}
	//Too many or no arguments supplied so output documentation
	else
	{
		cout << "Usage: ./Assignment2 [OPTION] [OPTION]\n\n";
		cout << "-f                  read from default file \"orderbundle.txt\"\n";
		cout << "-r                  read random order from Bundle object\n";
		cout << "-f -r (or -r -f)    to read from the random file instead of directly from Bundle object\n";
	}

	return 0;
}
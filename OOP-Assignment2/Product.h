/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.
I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#ifndef PRODUCT_H
#define PRODUCT_H
#include <iostream>
#include <array>
#include "Item.h"

using namespace std;

class Product: public Item
{
protected:
    array<int,4> components;
    int cycles;

public:
    Product(int i){id = i; cost = 0; setComponents(id); setCycles(id);}
    
    //Sets the components of the product according to the given id
    //Also sets the total cost of the product
    void setComponents(int i)
    {
        for (int j = 0; j < 4; ++j)
        {
            components[j] = compMap[i][j];
            Component tempComponent(compMap[i][j]);
            cost += tempComponent.getCost();
        }
    }

    //Sets cycles according to the cycle map in Item.h
    void setCycles(int i)
    {
        cycles = cycleMap[i];
    }

    int getCycles() const
    {
        return cycles;
    }

    int getCost() const
    {
        return cost;
    }

    //Returns the product's array of components
    array<int, 4> getComponents()
    {
        return components;
    }
};
#endif
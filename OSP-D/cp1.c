#include        <stdio.h>
#include        <unistd.h>
#include        <fcntl.h>
#include 		<sys/types.h>
#include 		<sys/stat.h>
#include		<stdlib.h>
#include		<string.h>
#include		<errno.h>
#define BUFFERSIZE      4096
#define COPYMODE        0644

void oops(char *, char *);
int dircheck(char *);

main(int ac, char *av[])
{
	int     in_fd, out_fd, n_chars, fileexists, folderfileexists;
	char    buf[BUFFERSIZE];
	char *	path;

	//Structs for holding info from stat()
	struct 	stat openbuffer, createbuffer, existbuffer;
	
	//Function for missing arguments
	if ( ac != 3 ){
		fprintf( stderr, "usage: %s source destination\n", *av);
		exit(1);
	}

	//Cannot open the first file
	if ( (in_fd=open(av[1], O_RDONLY)) == -1 )
		oops("Cannot open ", av[1]);

	//Get put info from stat() for both source and destination into structs
	stat(av[1], &openbuffer);
	fileexists = stat(av[2], &createbuffer);

	//Checks that both the source and destination are different
	if(openbuffer.st_ino != createbuffer.st_ino)
	{

		//Checks that the destination is not an existing directory
		if(!S_ISDIR(createbuffer.st_mode))
		{
			//Checks if the destination would be a directory, but does not exist
			if(dircheck(av[2]))
			{
				if(close(in_fd)==-1)
					perror(av[1]);

				oops("Directory does not exist. Aborting...\n",av[2]);
			}
			//Checks if destination file already exists
			if(fileexists == 0)
			{
				if(close(in_fd)==-1)
					perror(av[1]);

				errno = EEXIST;
				oops("Destination file already exists. Aborting...\n",av[2]);
			}
			
			//Routine for saving a file to the same directory
			if ( (out_fd=creat( av[2], COPYMODE)) == -1 )
				oops( "Cannot creat", av[2]);
	
			while ( (n_chars = read(in_fd , buf, BUFFERSIZE)) > 0 )
				if ( write( out_fd, buf, n_chars ) != n_chars )
					oops("Write error to ", av[2]);
			if ( n_chars == -1 )
				oops("Read error from ", av[1]);
	
			if(fchmod(out_fd,openbuffer.st_mode) == -1)
				oops("Permission setting error from ", av[2]);

			if ( close(in_fd) == -1 || close(out_fd) == -1 )
				oops("Error closing files","");
		}
		//Routine for saving file into a directory
		else
		{
			//Checks if trailing slash was provided and builds the file path accordingly
			if(dircheck(av[2]))
			{
				path = strcat(av[2],av[1]);
			}
			else
			{
				path = strcat(strcat(av[2],"/"),av[1]);
			}
			
			//Checks if a file with same exists in target folder
			folderfileexists=stat(path,&existbuffer);
			if(folderfileexists==0)
			{
				if(close(in_fd)==-1)
					perror(av[1]);

				errno = EEXIST;
				oops("File with same name exists in target directory. Aborting...\n",path);
			}
			
			if ((out_fd=creat(path, COPYMODE)) == -1)
				oops("Cannot creat", av[2]);

			while ( (n_chars = read(in_fd , buf, BUFFERSIZE)) > 0 )
				if ( write( out_fd, buf, n_chars ) != n_chars )
					oops("Write error to ", av[2]);
			if ( n_chars == -1 )
				oops("Read error from ", av[1]);
	
			if(fchmod(out_fd,openbuffer.st_mode) == -1)
				oops("Permission setting error from ", av[2]);

			if ( close(in_fd) == -1 || close(out_fd) == -1 )
				oops("Error closing files","");
		}
	}
	//If the file will be copied to itself
	else
	{
		if(close(in_fd)==-1)
			perror(av[1]);

		errno = EEXIST;
		oops("Attempted copy of file to itself. Aborting...\n",av[1]);
	}
}

//Error message
void oops(char *s1, char *s2)
{
	fprintf(stderr,"Error: %s ", s1);
	perror(s2);
	
	exit(1);
}

/*
  Checks if the trailing character of a string is a /
  Returns 1 if true
*/
int dircheck(char * string)
{
	return (string && *string && string[strlen(string) - 1] == '/') ? 1 : 0;
}
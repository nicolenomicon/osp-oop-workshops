/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.
I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <iostream>
#include "Tron.h"

void twoPlayerGame();

void onePlayerGame();

void zeroPlayerGame();

int main()
{
	int players;
	bool error;
	char playAgain;

	do
	{
		do
		{
			//Get game-mode
			std::cout << "What game mode do you want?\n1: 2 Human players\n2: 1 Human player, 1 AI player\n3: 2 AI players\n";
			std::cin >> players;
			std::cin.clear();
			std::cin.ignore();
			error = false;

			//Select game-mode
			switch(players)	
			{
				case 1:
				{
					twoPlayerGame();
					break;
				}

				case 2:
				{
					onePlayerGame();
					break;
				}

				case 3:
				{
					zeroPlayerGame();
					break;
				}	

				default:
					error = true;			
			}
		} while (error == true);

		//Play again?
		std::cout << "Would you like to play again? (Y/N) ";
		std::cin >> playAgain;	

	} while (playAgain == 'Y' || playAgain == 'y');

	return 0;
}

//Two human player game
void twoPlayerGame()
{
	int size = 0;
	int running = 0;
	char direction;

	//Get board size
	do
	{
		std::cout << "What should the length/height of the board be? (4-20) ";
		std::cin >> size;
		std::cin.clear();
		std::cin.ignore();
	} while (!(size <= 20) || !(size >=4));
	
	//Create board
	Board board(size);

	//Create two human players
	HumanPlayer player1('R', board.getSize());
	HumanPlayer player2('B', board.getSize());

	//Show the starting locations
	board.displayBoard();

	//Main game loop
	while (true)
	{
		//Get first player's move
		do
		{
			std::cout << "Which direction will you move, player 1? (u, d, l, r)";
			std::cin >> direction;
			std::cin.clear();
			std::cin.ignore();
		} while (direction != 'u' && direction != 'd' && direction != 'l' && direction != 'r');
		
		//Move the player
		player1.move(direction);
		
		//Update the board
		running = board.update(player1.getPosX(),player1.getPosY(),
			player1.getPrevPosX(),player1.getPrevPosY(),'R');
		
		//Display the board
		board.displayBoard();

		//Check for collisions 
		if (running == 1)
		{
			break;
		}

		//Get second player's move
		do
		{
			std::cout << "Which direction will you move, player 2? (u, d, l, r)";
			std::cin >> direction;
			std::cin.clear();
			std::cin.ignore();
		} while (direction != 'u' && direction != 'd' && direction != 'l' && direction != 'r');

		//Move the player
		player2.move(direction);

		//Update board state
		running = board.update(player2.getPosX(),player2.getPosY(),
			player2.getPrevPosX(),player2.getPrevPosY(),'B');

		//Display the board
		board.displayBoard();

		//Check for collisions 
		if (running == 1)
		{
			break;
		}
	}
}

//Human vs AI game
void onePlayerGame()
{
	int size = 0;
	int running = 0;
	char direction;

	//Get board size
	do
	{
		std::cout << "What should the length/height of the board be? (4-20) ";
		std::cin >> size;
		std::cin.clear();
		std::cin.ignore();
	} while (!(size <= 20) || !(size >=4));
	
	//Create board
	Board board(size);

	//Create one human player and one AI player
	HumanPlayer player1('R', board.getSize());
	AIPlayer player2('B', board.getSize());

	//Display starting locations
	board.displayBoard();

	//Main game loop
	while (true)
	{
		//Get human player's move
		do
		{
			std::cout << "Which direction will you move, player 1? (u, d, l, r)";
			std::cin >> direction;
			std::cin.clear();
			std::cin.ignore();
		} while (direction != 'u' && direction != 'd' && direction != 'l' && direction != 'r');

		//Move the player
		player1.move(direction);

		//Update the board
		running = board.update(player1.getPosX(),player1.getPosY(),
			player1.getPrevPosX(),player1.getPrevPosY(),'R');

		//Display the board
		board.displayBoard();

		//Check for collisions 
		if (running == 1)
		{
			break;
		}

		//Move the AI
		player2.move(board);

		//Update the board
		running = board.update(player2.getPosX(),player2.getPosY(),
			player2.getPrevPosX(),player2.getPrevPosY(), 'B');;

		//Display the board
		board.displayBoard();

		//Check for collisions 
		if (running == 1)
		{
			break;
		}
	}
}

//AI vs AI game
void zeroPlayerGame()
{
	int size = 0;
	int running = 0;

	//Get board size
	do
	{
		std::cout << "What should the length/height of the board be? (4-20) ";
		std::cin >> size;
		std::cin.clear();
		std::cin.ignore();
		std::cout << size;
	} while (!(size <= 20) || !(size >=4));
	
	//Create board
	Board board(size);

	//Create two AI players
	AIPlayer player1('R', board.getSize());
	AIPlayer player2('B', board.getSize());

	//Display starting locations
	board.displayBoard();

	//Main game loop
	while (true)
	{
		//Move the first player
		player1.move(board);

		//Update the board
		running = board.update(player1.getPosX(),player1.getPosY(),
			player1.getPrevPosX(),player1.getPrevPosY(),'R');

		//Display the board
		board.displayBoard();

		//Check for collisions 
		if (running == 1)
		{
			break;
		}

		//Move the second player
		player2.move(board);

		//Update the board
		running = board.update(player2.getPosX(),player2.getPosY(),
			player2.getPrevPosX(),player2.getPrevPosY(),'B');

		//Display the board
		board.displayBoard();

		//Check for collisions 
		if (running == 1)
		{
			break;
		}
	}
}
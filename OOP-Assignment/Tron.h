/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.
I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <iostream>
#include <cstdlib>
#include <time.h>

//Clas for the board
class Board
{
private:
	char **boardArray;
	int sizeOfArray;

public:
	/*Constructor takes the size of the arena and dynamically creates
	  a 2D array*/
	Board(int size)
	{
		sizeOfArray = size;
		boardArray = new char*[size];
		for (int i = 0; i < size; ++i)
		{
			boardArray[i] = new char [size];
		}

		//Initialises each element as a space
		for (int i = 0; i < size; ++i)
		{
			for (int j = 0; j < size; ++j)
			{
				boardArray[i][j] = ' ';
			}
		}
		//Initialises starting locations for players
		boardArray[1][1] = 'R';
		boardArray[size - 2][size - 2] = 'B';
	}

	//Destructor releases the memory used for the 2D array
	~Board()
	{
		for (int i = 0; i < sizeOfArray; ++i)
		{
			delete [] boardArray[i];
		}
		delete [] boardArray;
	}

	//Returns the length or height of the arena
	int getSize()
	{
		return sizeOfArray;
	}

	//Gets the value of an element of the array. If out-of-bounds, returns 'x'
	char getElement(int posX, int posY)
	{
		if (posX < 0 || posY < 0 || posX >= sizeOfArray || posY >= sizeOfArray)
		{
			return 'x';
		}
		return boardArray[posY][posX];
	}


	//Function for updating the board
	int update(int newPosX, int newPosY, int prevPosX, int prevPosY, char colour)
	{
		//Checks if Red player crashed into the perimeter
		if ((newPosX < 0 || newPosY < 0 || newPosX >= sizeOfArray || newPosY >= sizeOfArray) && colour == 'R')
		{
			std::cout << "Red player crashed into the perimeter! Blue player wins!\n";
			return 1;
		}
		//Checks if Blue player crashed into the perimeter
		else if ((newPosX < 0 || newPosY < 0 || newPosX >= sizeOfArray || newPosY >= sizeOfArray) && colour == 'B')
		{
			std::cout << "Blue player crashed into the perimeter! Red player wins!\n";
			return 1;
		}
		//Checks if both players crashed into eachother, and reflects that on the board
		else if (boardArray[newPosY][newPosX] == 'B' && colour == 'R')
		{
			std::cout << "You crashed into eachother! It's a tie.\n";
			boardArray[prevPosY][prevPosX] = 'O';
			boardArray[newPosY][newPosX] = 'X';
			return 1;
		}
		else if (boardArray[newPosY][newPosX] == 'R' && colour == 'B')
		{
			std::cout << "You crashed into eachother! It's a tie.\n";
			boardArray[prevPosY][prevPosX] = 'O';
			boardArray[newPosY][newPosX] = 'X';
			return 1;
		}
		//Checks if Red crashed into a player generated wall
		else if ((boardArray[newPosY][newPosX] != ' ') && colour == 'R')
		{
			std::cout << "Red player crashed into a wall! Blue player wins!\n";
			boardArray[prevPosY][prevPosX] = 'X';
			return 1;
		}
		//Checks if Blue crashed into a player generated wall
		else if ((boardArray[newPosY][newPosX] != ' ') && colour == 'B')
		{
			std::cout << "Blue player crashed into a wall! Blue player wins!\n";
			boardArray[prevPosY][prevPosX] = 'X';
			return 1;
		}
		//Otherwise update the player's position and add walls where they were
		else if (colour == 'R')
		{
			boardArray[newPosY][newPosX] = 'R';
			boardArray[prevPosY][prevPosX] = 'O';
			return 0;
		}
		else if (colour == 'B')
		{
			boardArray[newPosY][newPosX] = 'B';
			boardArray[prevPosY][prevPosX] = 'O';
			return 0;
		}			
	}

	//Displays the current board
	void displayBoard()
	{
		for (int i = 0; i < sizeOfArray; ++i)
		{
			//Builds top and cell horizontal boundaries
			for (int j = 0; j < sizeOfArray; ++j)
			{
				std::cout << " ---";
			}
			std::cout << "\n";
			//Builds vertical boundaries plus cell contents
			for (int j = 0; j < sizeOfArray; ++j)
			{
				std::cout << "| " << boardArray[i][j] << " ";
			}
			std::cout << "|\n";
		}
		//Builds bottom horizontal boundary
		for (int j = 0; j < sizeOfArray; ++j)
		{
			std::cout << " ---";
		}
		std::cout << "\n";
	}
};

//Parent class for players
class Player
{
//Getters
public:
	int getPosX()
	{
		return posX;
	}

	int getPosY()
	{
		return posY;
	}

	int getPrevPosX()
	{
		return prevPosX;
	}

	int getPrevPosY()
	{
		return prevPosY;
	}

//Data members
protected:
	int prevPosX, prevPosY, posX, posY, playerColour;
};

//Child class for human players
class HumanPlayer: public Player
{
public:
	//Sets starting position based on given colour and board size
	HumanPlayer(char colour, int boardSize)
	{
		playerColour = colour;
		if (playerColour == 'R')
		{
			posX = 1;
			posY = 1;
		}
		else
		{
			posX = boardSize - 2;
			posY = boardSize - 2;
		}
	}

	//Moves player based on given direction
	void move(char move)
	{
		//Records the previous position of the player
		prevPosX = posX;
		prevPosY = posY;

		//Changes current position based on input
		switch (move)
		{
			case 'u':
			{
				--posY;
				break;
			}
			case 'd':
			{
				++posY;
				break;
			}
			case 'l':
			{
				--posX;
				break;
			}
			case 'r':
			{
				++posX;
				break;
			}
		}
	}
};

//Child class for AI players
class AIPlayer: public Player
{
public:
	/*Sets starting postion based on given colour and board size.
	  Also sets the seed of the random number generator*/
	AIPlayer(char colour, int boardSize)
	{
		srand(time(0));
		playerColour = colour;
		if (playerColour == 'R')
		{
			posX = 1;
			posY = 1;
		}
		else
		{
			posX = boardSize - 2;
			posY = boardSize - 2;
		}
	}

	//Looks at available moves and makees one
	int move(Board &board)
	{
		int moves[4];
		int choice = 0;
		int count = 0;

		//Initialises the move array to -1's
		for (int i = 0; i < 4; ++i)
		{
			moves[i] = -1;
		}

		//Records player's current position
		prevPosX = posX;
		prevPosY = posY;

		//Counts up the number of moves that won't immediately kill the ai
		if (board.getElement(posX, posY + 1) == ' ')
		{
			moves[0] = 1;
			++count;
		}
		if (board.getElement(posX, posY - 1) == ' ')
		{
			moves[1] = 1;
			++count;
		}
		if (board.getElement(posX + 1, posY) == ' ')
		{
			moves[2] = 1;
			++count;
		}
		if (board.getElement(posX - 1, posY) == ' ')
		{
			moves[3] = 1;
			++count;
		}

		//If there are available moves generate that many random numbers
		if (count != 0)
		{
			choice = (rand() % count);
		}
		//Otherwise prioritise running into the other player to force a draw
		else
		{
			if (board.getElement(posX, posY + 1) == 'R' || board.getElement(posX, posY + 1) == 'B')
			{
				++posY;
				return -1;
			}
			if (board.getElement(posX, posY - 1) == 'R' || board.getElement(posX, posY - 1) == 'B')
			{
				--posY;
				return -1;
			}
			if (board.getElement(posX + 1, posY) == 'R' || board.getElement(posX + 1, posY) == 'B')
			{
				--posY;
				return -1;
			}
			if (board.getElement(posX - 1, posY) == 'R' || board.getElement(posX - 1, posY) == 'B')
			{
				--posY;
				return -1;
			}
			//No draw possible
			return -1;
		}

		//Loops until a valid move (if available) is chosen
		while (true)
		{
			//If the randomly generated move is valid, make that move
			if (moves[choice] == 1)
			{
				switch (choice)
				{
					case 0:
					{
						++posY;
						return 0;
					}

					case 1:
					{
						--posY;
						return 0;
					}
					
					case 2:
					{
						++posX;
						return 0;
					}
					
					case 3:
					{
						--posX;
						return 0;
					}
				}
			}
			//Otherwise generate a new move
			else
			{
				choice = (rand() % 4);
			}
		}
	}
};
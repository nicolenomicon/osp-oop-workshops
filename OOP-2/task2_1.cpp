//Return type wrong, type not given for each parameter
int total(int value1, value2, value3) {
	return value1 + value2 + value3;
}

/*Order of operations is wrong, integer division means
  it will floor the result and does not return anything*/
double average(int value1, int value2, int value3) {
	double average;
	average = value1 + value2 + value3 / 3;
}

/*Ampersand should be before the variable "int" is
  mispelt as "in" and ampersands are not required
  within the function itself*/
void getValue(in value&) {
	cout << "Enter a value:";
	cin >> value&;
}


/*You cannot overload functions just by changing the
  return type. You must also change the parameters*/

//overloaded functions
int getValue() {
	int inputValue;
	cout << "Enter an interger: ";
	cin >> inputValue;
	return inputValue;
}

double getValue() {
	double inputValue;
	cout << "Enter a floating-point number: ";
	cin >> inputValue;
	return inputValue;
}
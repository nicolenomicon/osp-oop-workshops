#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char const *argv[])
{
	double width, distance, calculatedWidth,
		calculatedDistance, finalWidth, finalDistance;
	int underwaterCost, landCost, totalCost, lowestCost;

	//Gets user input
	cout << "Enter the width of the	river (metres): ";
	cin >> width;
	cout << "Enter the distance to the factory (metres): ";
	cin >> distance;
	cout << "Enter the cost of laying the power line underwater: ";
	cin >> underwaterCost;
	cout << "Enter the cost of laying the power line over land: ";
	cin >> landCost;

	//Initialises variables
	totalCost = 0;
	lowestCost = 0;

	/*Loop iterates through each subtracted metre
	  of distance*/
	for (int i = 0; i <= distance; ++i)
	{
		/*Calculates the new distance from the other 
		  side of the river to the factory*/
		calculatedDistance = distance - i;
		/*Calculates distance accross the river to this
		  new point according to pythagoras' theorem*/
		calculatedWidth = sqrt(pow(width,2) + pow(i,2));

		//Calculates the cost from these measurements
		totalCost = (calculatedDistance * landCost)
			+ (calculatedWidth * underwaterCost);

		/*If this is the first run through, or a
		  new lowest cost has been found execute this*/
		if (totalCost < lowestCost || i == 0)
		{
			/*Store the new lowest cost and its 
			  associated measurements*/
			lowestCost = totalCost;
			finalDistance = calculatedDistance;
			finalWidth = calculatedWidth;
		}
	}
	
	//Outputs the cable lengths and the total cost
	cout << "Suggested length of land cable: " << finalDistance << " metres.\n";
	cout << "Suggested length of underwater cable: " << finalWidth << " metres.\n";
	cout << "Total cost of construction: $" << lowestCost << ".\n";
	return 0;
}
#include <iostream>

class TestClass
{
	public:
		TestClass()
		{
			std::cout << "This is the constructor of the test class\n";
		}
};

int main()
{
	class TestClass test;
	return 0;
}
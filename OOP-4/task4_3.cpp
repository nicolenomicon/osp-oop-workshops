#include <iostream>

using namespace std;

class Plane
{
public:
	Plane()
	{
		for (int i = 0; i < 7; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				switch (j)
				{
					case 0:
						seatingArray[i][j] = 'A';
						break;
					case 1:
						seatingArray[i][j] = 'B';
						break;
					case 2:
						seatingArray[i][j] = 'C';
						break;
					case 3:
						seatingArray[i][j] = 'D';

				}
			}
		}
	}

	bool checkAvailability()
	{
		int seatCount = 0;
		for (int i = 0; i < 7; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				if (seatingArray[i][j] == 'X')
				{
					seatCount++;
				}
			}
		}
		if (seatCount == 28)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	void printSeats()
	{
		for (int i = 0; i < 7; ++i)
		{
			cout << i + 1;
			for (int j = 0; j < 4; ++j)
			{
				cout << " " << seatingArray[i][j];
			}
			cout << endl;
		}
	}

	void bookSeat()
	{
		int row;
		char column;
		bool error;
		do
		{
			if (error)
			{
				cout << "Seat already taken or invalid seat entered. Please select again." << endl;
			}
			error = false;
			cout << "Which row would you like (1-7)? ";
			cin >> row;
			row = row - 1;
			cout << "which column would you like (A-D)? ";
			cin >> column;
			switch (column)
			{
				case 'A':
					if (seatingArray[row][0] != 'X')
					{
						seatingArray[row][0] = 'X';
					}
					else
					{
						error = true;
					}
					break;

				case 'B':
					if (seatingArray[row][1] != 'X')
					{
						seatingArray[row][1] = 'X';
					}
					else
					{
						error = true;
					}
					break;

				case 'C':
					if (seatingArray[row][2] != 'X')
					{
						seatingArray[row][2] = 'X';
					}
					else
					{
						error = true;
					}
					break;

				case 'D':
					if (seatingArray[row][3] != 'X')
					{
						seatingArray[row][3] = 'X';
					}
					else
					{
						error = true;
					}
					break;
				default:
					error = true;
			}
		} while (error);
	}

private:
	char seatingArray[7][4];
};

int main(int argc, char const *argv[])
{
	char response;
	class Plane testPlane;
	bool available;

	do
	{
		available = testPlane.checkAvailability();
		if (available)
		{
			testPlane.printSeats();
			testPlane.bookSeat();
			testPlane.printSeats();
			cout << "Continue booking (y/n)? ";
			cin >> response;
		}
		else
		{
			cout << "Flight fully booked." << endl;
		}
		
	} while (response == 'y' || available);
	
	return 0;
}
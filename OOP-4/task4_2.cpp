#include <iostream>

using namespace std;

class Student
{
	private:
		int studentId, courseNo;
		string studentName, enrolmentStatus;
	public:
	Student()
	{
		cout << "Enter student ID: ";
		cin >> studentId;
		cin.ignore();
		cout << "Enter student name: ";
		getline(cin, studentName);
		cout << "Enter course number: ";
		cin >> courseNo;
		cout << "Enter enrolment status: ";
		cin >> enrolmentStatus;
	}
	void display()
	{
		cout << "Student ID: " << studentId << endl;
		cout << "Name: " <<  studentName << endl;
		cout << "Course Number: " << courseNo << endl;
		cout << "Enrolment status: " << enrolmentStatus << endl;
	}
};

int main()
{
	class Student studentOne;
	studentOne.display();
	return 0;
}
#include <iostream>

using namespace std;

int main()
{
	int denominator;
	double total = 0;
	
	cout << "Enter final denominator: ";
	if (cin >> denominator)
	{
		if (denominator > 0)
		{
			for(int i = 1; i <= denominator; i++)
			{
				total = total + (double) 1/i;
			}
			cout << total << "\n";
			return 0;
		}
	}
	cout << "Not a valid positive integer.\n";
	return -1;
}
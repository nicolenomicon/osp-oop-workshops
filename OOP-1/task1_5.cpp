#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
	int month, day;
	cout << "Welcome to your friendly weather program.\n"
		<< "Enter today's date as two integers for the month and the day:\n";
	cin >> month;
	cin >> day;
	srand(month*day);
	int prediction;
	char ans;
	cout << "Weather for today:\n";
	do
	{
		prediction = rand() % 10;
		if (prediction < 5)
		{
			cout << "The day will be sunny!!\n";
		}
		else if (prediction < 9)
		{
			cout << "The day will be cloudy.\n";
		}
		else if (prediction == 9)
		{
			cout << "The day will be stormy!.\n";
		}
		else
		{
			cout << "Weather program is not functioning properly.\n";
		}
		cout << "Want the weather for the next day?(y/n): ";
		cin >> ans;
	} while (ans == 'y' || ans == 'Y');
	cout << "That's it from your 24 hour weather program.\n";
	return 0;
}
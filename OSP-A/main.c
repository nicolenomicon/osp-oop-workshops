#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

//OSP Workshop A, version 1 spec - Nicholas Featherby 17694357

/* handy typedefs */
typedef unsigned char card;
typedef unsigned char pairs;

/* arrays for the names of things */
static char *suits[] = { "Hearts", "Diamonds", "Clubs", "Spades" };
static char *values[] = { "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
static char *colour[] = { "Black", "Red" };

/* function prototypes */
void printcard(card c); /* Displays the value of a card*/

void printdeck(card deck[52]); /* prints an entire deck of cards*/

void filldeck(card deck[52]); /* Populates a deck of cards */

void shuffle(card deck[52]); /* Randomizes the order of cards */

int compareface(const void* c1, const void *c2);
/* compares the face value of 2 cards, suitable to pass to qsort
as the fourth argument */

pairs findpairs(card *hand); /* finds any pairs in a hand */

//Main function
int main()
{
	card deck[52], handValues[5];
	card hands[5][5], handssorted[5][5];
	pairs numpairs[5];
	int nextCard = 0;
	int hand, cd, draw;
	pairs pairResults, winningpair, besthand;

	srand(time(NULL));       /* seed the random number generator */


	/*populate and shuffle the deck */
	filldeck(deck);
	printdeck(deck);
	printf("\n ~~~~~~~~~~~~~~~~~~*****SHUFFLING*****~~~~~~~~~~~~~~~~~ \n \n");
	shuffle(deck);
	printdeck(deck);

	//Deal hands
	for (cd = 0; cd < 5; cd++)
	{
		for (hand = 0; hand < 5; hand++)
		{
			/* deal the hands here */
			hands[hand][cd] = deck[nextCard];
			nextCard++;
		}
	}

	//Sort hands
	besthand = 0;
	winningpair = 0;
	for (hand = 0; hand < 5; hand++)
	{
		/* sort the hands here */

		//Gets all the cards in the current hand
		for (cd = 0; cd < 5; cd++)
		{
			handValues[cd] = hands[hand][cd];
		}
		//sorts the current hand
		qsort(handValues, 5, sizeof(char), compareface);
		//Places the sorted hand into the correct position of the handssorted array
		for (cd = 0; cd < 5; cd++)
		{
			handssorted[hand][cd] = handValues[cd];
		}
		printf("\n");

		//gets all the pairs for the current hand
		numpairs[hand] = findpairs(handssorted[hand]);
		printf("\n");

		/* print the hands here */
		printf("\nHand %i: \n", hand + 1);
		for (cd = 0; cd < 5; cd++)
		{
			printf("	");
			printcard(handssorted[hand][cd]);
		}

		/* print the number and value of any pairs here */

		//Pull out the number of pairs in the current hand
		pairResults = numpairs[hand];
		pairResults = pairResults & 15;

		//Prints out the number of pairs for the hand and what value pair it is
		if (pairResults == 0)
		{
			printf("Number of pairs: 0 \n");
		}
		else
		{
			printf("Number of pairs: %d \n", pairResults);
			pairResults = numpairs[hand];
			pairResults = pairResults >> 4;
			pairResults = pairResults & 15;
			printf("Highest pair is: %s \n", values[pairResults]);
			//Keeps track of what the highest pair so far is
			if (winningpair < pairResults)
			{
				winningpair = pairResults;
				besthand = hand + 1;
			}
			//Accounts for two hands having same value pairs
			else if (winningpair == pairResults && winningpair != 0)
			{
				draw = 1;
			}
		}

	}

	/* determine the winner and print it */

	//Prints out the draw
	if (draw == 1 || besthand == 0)
	{
		printf("Drawn game \n");
	}
	//Prints out the winning hand and its value
	else
	{
		printf("Winner is hand %d with a pair of %ss \n", besthand, values[winningpair]);
	}

	//For running from Visual Studio
	system("pause");

	return 0;
}

/*Function for finding pairs in a given hand Returns numpairs*/
pairs findpairs(card *hand)
{
	pairs numpairs = 0;
	int cardCount;
	int nextcardcount;
	int pairCount = 0;
	card testPair[5];
	card firstCard;
	card secondCard;
	card highestValue;

	/* find the pairs here */

	//Populates the testing array
	for (cardCount = 0; cardCount < 5; cardCount++)
	{
		testPair[cardCount] = hand[cardCount];
	}

	//Compares all cards ignoring self comparisons and multiple comparisons of the same two cards
	for (cardCount = 0; cardCount < 5; cardCount++)
	{
		for (nextcardcount = cardCount + 1; nextcardcount < 5; nextcardcount++)
		{
			//Right shifts for value masking
			firstCard = testPair[cardCount] >> 2;
			firstCard = firstCard & 15;
			//Shifts back to follow card spec
			firstCard = firstCard << 2;
			//Right shifts for value masking
			secondCard = testPair[nextcardcount] >> 2;
			secondCard = secondCard & 15;
			//Shifts back to follow card spec
			secondCard = secondCard << 2;

			//Checks if cards have the same value. Increases number of pairs and records the highest pair so far
			if (firstCard == secondCard)
			{
				highestValue = firstCard;
				pairCount++;
			}
		}
	}

	//Prevents a 3-of-a-kind from being counted as more than a pair
	if (pairCount == 3)
	{
		pairCount = 1;
	}
	//Prevents a full house or four-of-a-kind from being counted as more than two pairs
	else if (pairCount == 5 || pairCount == 6)
	{
		pairCount = 2;
	}

	//Adds the number of pairs of this hand to numpairs
	numpairs = numpairs + pairCount;
	//Left shifts highest value to match the "pairs" spec and adds it to numpairs
	highestValue = highestValue << 2;
	numpairs = numpairs + highestValue;
	return numpairs;
}

/*Fills the deck with all of the cards*/
void filldeck(card deck[52])
{
	/* populate the deck here */

	int x = 0;

	for (x = 0; x < 13; x++)
	{
		//Adds all the hearts to the deck
		deck[x] = 64 + x * 4;
	}
	for (x = 0; x < 13; x++)
	{
		//Adds all the diamonds to the deck
		deck[x + 13] = 64 + x * 4 + 1;
	}
	for (x = 0; x < 13; x++)
	{
		//Adds all the clubs to the deck
		deck[x + 26] = x * 4 + 2;
	}
	for (x = 0; x < 13; x++)
	{
		//Adds all the spades to the deck
		deck[x + 39] = x * 4 + 3;
	}
	return;
}

/*Prints out the whole deck*/
void printdeck(card deck[52])
{
	int i;
	for (i = 0; i < 52; i++)
		printcard(deck[i]);
	return;
}

/*Prints out an indiviual card*/
void printcard(card c)
{
	/* print the value of the card here */
	card testCard = c;
	int x = 0;
	int cardSuit = 0;
	int cardValue = 0;
	int cardColour = 0;

	//Checks for invalid card
	if (c == 0)
	{
		printf("Invalid Card \n");
	}
	else
	{
		//Shifts the colour bit and masks it for ease of checking. Is then placed in cardColour
		testCard = testCard >> 6;
		testCard = testCard & 1;
		cardColour = testCard;
		//Masks out all bits but the suit and assigns that to cardSuit
		testCard = c;
		testCard = testCard & 3;
		cardSuit = testCard;

		//Shifts the value bits and masks them for ease of checking. Placed in
		testCard = c;
		testCard = testCard >> 2;
		testCard = testCard & 15;
		cardValue = testCard;

		//Prints the card
		printf("%s of %s, is %s\n", values[cardValue], suits[cardSuit], colour[cardColour]);
	}

	return;
}

/*Shuffles the deck*/
void shuffle(card deck[52])
{
	int i, rnd;
	card c;

	for (i = 0; i < 52; i++)
	{
		/* generate a random number between 0 & 51 */
		rnd = rand() * 52.0 / RAND_MAX;
		c = deck[rnd];
		deck[rnd] = deck[i];
		deck[i] = c;
		/* finish shuffling the deck here */
	}

	return;
}

/*Compares the faces of two given cards*/
int compareface(const void* c1, const void *c2)
{
	/* This function extracts the two cards face values
	and returns 1 if cd1 > cd2, 0 if cd1 == cd2, and
	-1 otherwise. The weird argument types are for
	compatibility with qsort(), the first two lines
	decode the arguments back into "card".
	*/
	card cd1, cd2;

	cd1 = *((card*)c1);
	cd2 = *((card*)c2);

	cd1 = (cd1 & 0x3c) >> 2;
	cd2 = (cd2 & 0x3c) >> 2;

	if (cd1 > cd2)
		return 1;
	if (cd1 == cd2)
		return 0;

	return -1;
}
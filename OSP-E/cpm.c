/* cpm.c
   Useful functions for Workshop E 
*/
#include "cpm.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
unsigned char bitmap[12];
struct CPMdirent directory[128];
//static const struct CPMdirent emptystruct;
int selectionint;
char buffer[256];

int main()
{
	int opendir = -1;
	int initialised = 0;
	while(1)
	{
		selectionint = 0;
		printf("Select from these options:\n1: Initialise Disk\n2: List Files\n3: Display Bitmap\n4: Open/Create File\n5: Read File\n6: Write File\n7: Delete File\n8: Exit CPM\nEnter your selection here: \n");
		//Menu selection
		while(selectionint == 0)
		{
			if(scanf("%d", &selectionint) != 1)
			{
				fgets(buffer,256,stdin);
				printf("Select from these options:\n1: Initialise Disk\n2: List Files\n3: Display Bitmap\n4: Open/Create File\n5: Read File\n6: Write File\n7: Delete File\n8: Exit CPM\nEnter your selection here: \n");
			}
		}
		//Clearing of errant \n's
		fgets(buffer,256,stdin);

		//Menus
		switch(selectionint)
		{
			case 1:
				initialised = initialise(initialised);
				printf("\n");
				break;

			case 2:
				listfiles();
				printf("\n");
				break;

			case 3:
				displaybitmap();
				printf("\n");
				break;

			case 4:
				opendir = opencreatefile();
				printf("\n");
				break;

			case 5:
				if(opendir != -1)
					readfile(opendir);
				else
					printf("No file currently open to be read");
				printf("\n");
				break;

			case 6:
				if(opendir != -1)
					writefile(opendir);
				else
					printf("No file currently open to be written to");
				printf("\n");
				break;

			case 7:
				if(opendir != -1)
				{
					deletefile(opendir);
					opendir = -1;
				}
				else
					printf("No file currently open to be deleted");
				printf("\n");
				break;
			case 8:
				exit(0);
		}
	}
}

//Initialises the bitmap and disk
int initialise(initialised)
{
	int blockcount, dircount;

	//Sets all blocks but the 0th to be free
	for(blockcount = 1; blockcount<90;blockcount++)
	{
		toggle_bit(blockcount);
	}
	//Sets all files to be free
	for(dircount = 0; dircount < 128; dircount++)
	{
		directory[dircount].usercode = -1;
	}

	//If the disk has already been initialised once, it needs to be run twice
	if(initialised == 1)
	{
		for(blockcount = 1; blockcount<90;blockcount++)
		{
			if(block_status(blockcount) == 0)
				toggle_bit(blockcount);
		}
		//Sets all files to be free
		for(dircount = 0; dircount < 128; dircount++)
		{
			directory[dircount].usercode = -1;
		}
	}
	
	initialised = 1;
	
	return initialised;
}

//Lists all files on the disk
int listfiles()
{
	int dircount;
	printf("Files on disk: \n");

	//Loops through all directory entries
	for(dircount = 0; dircount < 128; dircount++)
	{
		//If the usercode isn't -1 it is a valid directory entry
		if(directory[dircount].usercode != -1)
		{
			printf("%s.%s	Size: %d\n", directory[dircount].filename, directory[dircount].filetype, directory[dircount].blockcount);
		}
	}
}

//Displays the current bitmap
int displaybitmap()
{
	int blockcount, blockstatus;

	printf("Allocation bitmap: \n");

	//Loops through all blocks
	for(blockcount = 0; blockcount < 90; blockcount++)
	{
		//Gets the value of the block
		blockstatus = block_status(blockcount);

		//Encodes the status in binary
		if(blockstatus == 0)
			printf("0 ");
		else
			printf("1 ");
		
		//Cleanliness
		if(blockcount == 29 || blockcount == 59)
			printf("\n");
	}
	printf("\n");
}

//Opens an existing file, or creates a new one
int opencreatefile()
{
	char filename[10];
	char filetype[5];
	int dircount, opendir, availableblock, validinput, blockcount;
	
	
	validinput = 0;

	//Loops until a filename is given
	while(validinput != 1)
	{
		printf("Filename: \n");

		fgets(filename, sizeof(filename), stdin);
		if(strcmp(filename, "\n\0") != 0)
		{

			validinput = 1;
		}
	}
	
	validinput = 0;

	//Loops until a filetype is given
	while(validinput != 1)
	{
		printf("Filetype (eg. 'dir', 'txt'): \n");

		fgets(filetype, sizeof(filetype), stdin);

		if(strcmp(filetype, "\n\0") != 0)
			validinput = 1;
	}
	
	//Removes \n from the given strings
	filename[strlen(filename) - 1] = '\0';
	filetype[strlen(filetype) - 1] = '\0';

	//Loops through all directories
	for(dircount = 0; dircount < 128; dircount++)
	{
		//Finds the directory with the given name,type, and makes sure it exists
		if((strcmp(directory[dircount].filename, filename) == 0) && (strcmp(directory[dircount].filetype, filetype) == 0) && directory[dircount].usercode == 1)
		{
			//Found the file, store it's position in the struct array
			printf("Opening %s.%s...\n", directory[dircount].filename, directory[dircount].filetype);
			
			return dircount;
		}
	}

	//Could not find the given file. Will now create it
	printf("%s.%s not found. Creating...\n", filename,filetype);

	//Checks that there are blocks available to be allocated
	for(blockcount = 1; blockcount < 90; blockcount++)
	{
		if(block_status(blockcount) != 0)
			break;
		else if(blockcount == 89)
		{
			printf("No available blocks. Aborting...\n");
			return(-1);
		}	
	}

	//Looks for the first directory with an invalid user code
	for(dircount = 0; dircount < 128; dircount++)
	{
		if(directory[dircount].usercode == -1)
		{
			//Sets its user code to be valid
			directory[dircount].usercode = 1;
			
			//Applies name, filetype, and other properties
			strcpy(directory[dircount].filename, filename);
			strcpy(directory[dircount].filetype, filetype);
			directory[dircount].extent = 1;
			return dircount;
			//directory[dircount].blockcount = 1;

			//Allocates first available block to the file
			/*for(availableblock = 1; availableblock < 89; availableblock++)
			{
				if(block_status(availableblock) != 0)
				{
					directory[dircount].blocks[0] = availableblock;
					toggle_bit(availableblock);
					return dircount;
				}
			}*/
			
		}
	}

	//Too many files exist
	printf("Maximum files on disk. Aborting...\n");
	return(-1);
}

//Displays which blocks are allocated to the current file
int readfile(int opendir)
{
	int blockcount;
	printf("Blocks occupied by %s.%s: \n", directory[opendir].filename,directory[opendir].filetype);
	for(blockcount = 0; blockcount < directory[opendir].blockcount; blockcount++)
	{
		printf("%d ", directory[opendir].blocks[blockcount]);
	}
}

//Allocates new blocks to the file
int writefile(int opendir)
{
	int blockcount;

	//Checks that there are blocks available
	for(blockcount = 1; blockcount < 90; blockcount++)
	{
		if(block_status(blockcount) != 0)
			break;
		else if(blockcount == 89)
		{
			printf("No available blocks. Aborting...\n");
			return(-1);
		}	
	}

	//16 block limit
	if(directory[opendir].blockcount == 16)
	{
		printf("Reached maximum size for: %s.%s\nAborting...\n",directory[opendir].filename,directory[opendir].filetype);
		return(-1);
	}
	
	//Finds the next available block
	printf("Writing to next available block for %s.%s...\n", directory[opendir].filename,directory[opendir].filetype);
	for(blockcount = 1; blockcount < 90; blockcount++)
	{
		if(block_status(blockcount) != 0)
		{
			//Toggles the block, adds the new block to the file, increases block count for the file
			toggle_bit(blockcount);
			directory[opendir].blocks[directory[opendir].blockcount] = blockcount;
			directory[opendir].blockcount = directory[opendir].blockcount + 1;
			return(0);
		}
	}
}

//Deletes the currently opened file
int deletefile(int opendir)
{
	int blockcount;

	//Loops through all blocks allocated to the file
	for(blockcount = 0; blockcount < directory[opendir].blockcount; blockcount++)
	{	
		//Toggles allocated block
		toggle_bit(directory[opendir].blocks[blockcount]);
	}
	//Empties the struct's contents and gives it an invalid user code
	//directory[opendir] = emptystruct;
	directory[opendir].usercode = -1;
}

//Toggles a bit to 0 or 1
int toggle_bit(int block)
{
	int elem=block/8;
	int pos=block%8;
	int mask=1<<pos;
	
	bitmap[elem]^=mask;

	return bitmap[elem]&mask;
}

//Returns the value of a bit
int block_status(int block)
{
	int elem=block/8;
	int pos=block%8;
	int mask=1<<pos;

	return bitmap[elem]&mask;
}
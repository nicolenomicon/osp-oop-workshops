/* logserver.c -- implementation of the log server */
#include <signal.h>
#include "logservice.h"

void signalhandler(int signalnumber);

int main()
{
	struct message msg;
	printf("Logging server starting...\n");
	int id,debug;

	//Debug lines for clearing queue in case of problems
	debug = 0;
	if(debug == 1)
	{
		id = msgget(KEY, 0666);
		msgctl(id, IPC_RMID, 0);
	}
	//Create the queue
	else
	{
		id = msgget(KEY, 0666 | IPC_CREAT | IPC_EXCL);	
	}
	//Fail if the queue couldn't be made
	if(id == -1)
	{
		perror("Failed to create queue");
		exit(-1);
	}

	printf("Queue number is %d\n", id);

	if(debug == 0)
	{
		//Loop for receiving messages
		while(1)
		{
			//Signal handler for ctrl c interrupt
			signal(SIGINT, signalhandler);

			if((msgrcv(id, &msg, sizeof(struct message),0, 0)) < 0 )
			{
				perror("Message receiving");
				exit(1);
			}
			printf("%ld: %s\n",msg.type, msg.message);
		}	
	}
	return 0;
}
//Signal handler function for clearing queue
void signalhandler(int signalnumber)
{
	if(signalnumber == SIGINT)
	{
		int id = msgget(KEY,0666);
		msgctl(id, IPC_RMID, 0);
		exit(0);
		
	}
}
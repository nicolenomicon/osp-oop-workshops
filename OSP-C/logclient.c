/* logclient.c -- implements a simple log service client */
#include "logservice.h"

int main(int argc,char**argv)
{
	int logServiceInit();
	int logMessage(int serviceId, char*message);
	int result,id;
	pid_t pid = getpid();
	struct message msg;
	char inputmessage[MSGCHARS];

	//Connect to logging queue
	printf("Logging service starting...\n");
	id = logServiceInit();
	if(id == -1)
	{
		exit(-1);
	}
	printf("Queue id is %d\n", id);

	//Message sending loop
	while(1)
	{
		//Get message from user
		fgets(inputmessage,MSGCHARS,stdin);
		//Add pid to message
		msg.type = (long)pid;
		//Add user's message to the message
		strcpy(msg.message,inputmessage);
		//Send the message
		result = logMessage(msg.type, msg.message);
		//Error handling
		if(result != 0)
		{
			perror("Messege sending");
		}
	}
	return 0;
}


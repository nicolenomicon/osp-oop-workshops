/* logservice.c -- implementation of the log service */
#include "logservice.h"
#include <errno.h>
int logServiceInit()
{
	int id;
	//Gets the id for the queue
	if((id = msgget(KEY, 0666)) == -1)
	{
		perror("Failed to connect to queue");
		return -1;
	}
	else
	{
		
		return id;
	}
	

}

//Function for constructing and sending message
int logMessage(int serviceId,char*message)
{
	int rv,id;
	struct message msg;

	//Get queue id
	id = logServiceInit();

	//Construct message
	msg.type = serviceId;
	strcpy(msg.message,message);

	//Send message
	msgsnd(id,&msg,sizeof(msg),0);
	if(errno == 0)//it worked
	{
		rv = 0;
		return rv;
	}
	else//something went wrong
	{
		rv = -1;
		return rv;
	}
	
}

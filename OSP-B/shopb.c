#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>

#define MAX_LINE 4096
#define MAX_WORDS MAX_LINE/2
/* a line can have at most MAX_LINE/2 words, why? */

void tokenize(char *line, char **words, int *nwords);
/* break line into words separated by whitespace, placing them in the 
   array words, and setting the count to nwords */

int main()
{
        char line[MAX_LINE], *words[MAX_WORDS], message[MAX_LINE];
        int stop=0,nwords=0;
        char* program = "cd";
        char* currentworkingdirectory;
        char buff[PATH_MAX + 1];
        int arraysize, validentry;
        int status = 0;
        pid_t pid, child, wpid;
                
        while(1)
        {
            validentry = 0;
            currentworkingdirectory = getcwd(buff, PATH_MAX + 1);
            /* read a line of text here */
            while(line[0] == '\0' || validentry == 0 || line[0] == '\n')
            {
                if(validentry == 1)
                {
                    printf("Unknown command\n");
                }
                printf("OSP CLI %s $ ",currentworkingdirectory);
                fgets(line,MAX_LINE,stdin);
                tokenize(line,words,&nwords);
                validentry = 1;
			}

            program = words[0];
            

            /* More to do here */
            //Exit program
            if(strcmp(line,"exit")==0)
            {
                exit(0);
            }
            //cd implementation
            else if (strcmp(words[0],"cd")==0)
            {
                chdir(words[1]);
                perror("Change directory");
            }
            //executing other programs
            else
            {
                switch(child=fork())
                {
                    //Error forking
                    case -1:
                        perror("fork");
                        exit(1);
                        break;
                    //Run program in child process
                    case 0:
                        if(execvp(program,words))
                        {
                            perror(words[0]);
                            exit(1);
                        }
                        else
                        {
                            exit(0);
                        }
                        break;
                    //Wait for child to finish and print exit status of child
                    default:
                        while((wpid = wait(&status)) > 0 )
                        {
                            
                            printf("Exit status of %d was %d (%s)\n", (int)wpid, WEXITSTATUS(status),(status > 0) ? "failure" : "success");
                        }
                        break;
                }
            }            
        }
        return 0;
}

/* this function works, it is up to you to work out why! */
void tokenize(char *line, char **words, int *nwords)
{
        *nwords=1;

        for(words[0]=strtok(line," \t\n");
            (*nwords<MAX_WORDS)&&(words[*nwords]=strtok(NULL, " \t\n"));
            *nwords=*nwords+1
           ); /* empty body */
        return;
}

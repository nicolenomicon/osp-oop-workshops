//REQUIRES C++11
//g++ -std=c++11 task7_3.cpp -o task7_3

#include <fstream>
#include "Date.h"
#include <random>

using namespace std;

int main(int argc, char const *argv[])
{
	default_random_engine generator;
	uniform_int_distribution<int> randDay(1,31);
	uniform_int_distribution<int> randMonth(1,12);
	uniform_int_distribution<int> randYear(2000,2002);
	
	ofstream fout;

	fout.open("7_3.txt");
	if (fout.fail()) {
		cout << "Output file opening failed.\n";
		exit(1);
	}

	for (int i = 0; i < 100; ++i)
	{
		int day = randDay(generator);
		int month = randMonth(generator);
		int year = randYear(generator);
		Date d(day, month, year);
		
		//Makes sure it's a real date
		if (!((day > 27 && month >= 9 && year == 2002) || (month > 9 && year == 2002) || 
			(day > 28 && month == 2 && year != 2000) || (day > 29 && month == 2 && year == 2000) || 
			(day == 31 && (month == 4 || month == 6 || month == 9))))
		{
			fout << setfill('0') << setw(2) << day << "/";
			fout << setfill('0') << setw(2) << month << "/" ;
			fout << year << ",";
		}
		//If it isn't, decrease the count so the correct # of dates is generated
		else
		{
			i--;
		}
		
	}

	return 0;
}
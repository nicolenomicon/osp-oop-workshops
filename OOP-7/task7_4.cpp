//REQUIRES C++11
//g++ -std=c++11 task7_4.cpp -o task7_4

#include "Date.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
	ifstream filein;

	//Gets dates generated from task 7.3
	filein.open("7_3.txt");
	if (filein.fail()) {
		cout << "Input file opening failed.\n";
		exit(1);
	}

	//Vector for all the dates
	vector<Date> dateVector;

	//Adds each date from the file to the vector
	while (!filein.eof())
	{
		char delimiter;
		int day, month, year;
		filein >> day;
		filein.get(delimiter);
		filein >> month;
		filein.get(delimiter);
		filein >> year;
		filein.get(delimiter);
		Date tempDate(day,month,year);
		dateVector.push_back(tempDate);
	}

	//Vector for each season
	vector<Date> summerVector;
	vector<Date> autumnVector;
	vector<Date> winterVector;
	vector<Date> springVector;

	//Goes through each date in original vector, checks which season it's in and adds it to the correct vector
	for (int i = 0; i < dateVector.size(); ++i)
	{
		if (dateVector[i].getSeason() == SUMMER)
		{
			summerVector.push_back(dateVector[i]);
		}
		else if (dateVector[i].getSeason() == AUTUMN)
		{
			autumnVector.push_back(dateVector[i]);
		}
		else if (dateVector[i].getSeason() == WINTER)
		{
			winterVector.push_back(dateVector[i]);
		}
		else
		{
			springVector.push_back(dateVector[i]);
		}
	}

	//Open a file for writing results
	ofstream seasonOutput;
	seasonOutput.open("season_output.txt");
	if (seasonOutput.fail()) {
		cout << "Output file opening failed.\n";
		exit(1);
	}

	//Writes out all the summer dates to the file
	seasonOutput << "Summer:" << endl;
	for (int i = 0; i < summerVector.size(); ++i)
	{
		seasonOutput << summerVector[i] << endl;
	}

	//Writes out all the autumn dates to the file
	seasonOutput << "Autumn:" << endl;
	for (int i = 0; i < autumnVector.size(); ++i)
	{
		seasonOutput << autumnVector[i] << endl;
	}

	//Writes out all the winter dates to the file
	seasonOutput << "Winter:" << endl;
	for (int i = 0; i < winterVector.size(); ++i)
	{
		seasonOutput << winterVector[i] << endl;
	}

	//Writes out all the spring dates to the file
	seasonOutput << "Spring:" << endl;
	for (int i = 0; i < springVector.size(); ++i)
	{
		seasonOutput << springVector[i] << endl;
	}

	return 0;
}
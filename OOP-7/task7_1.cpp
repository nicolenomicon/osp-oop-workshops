#include <iostream>

using namespace std;

class TestClass
{
public:
	TestClass()
	{
		totalObjects++;
	}

	int getTotal()
	{
		return totalObjects;
	}
private:
	static int totalObjects;
};

int TestClass::totalObjects=0;

int main(int argc, char const *argv[])
{
	TestClass test;
	cout << test.getTotal() << endl;
	TestClass testTwo;
	cout << test.getTotal() << endl;
	cout << testTwo.getTotal() << endl;
	return 0;
}
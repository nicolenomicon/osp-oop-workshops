#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>

int main()
{
         pid_t child, wpid;
         int status = 0;
         switch(child=fork())
         {
         case -1:
               perror("fork");
               exit(1);
               break;
         case 0:
               printf("I am the child my pid is: %d\n",getpid());
               exit(0);
               break;
         default:
               while ((wpid = wait(&status)) > 0)
               {
                printf("I am the parent, my pid is %d," ,getpid());
                printf("and my childs is %d\n",child);
                exit(0);
                break;
               }
               
        }
        return 0;
}


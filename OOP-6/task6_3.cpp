#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

//Parent class
class Product
{
public:
	//Default constructor
	Product(string b = "barcode", string p = "Default Product") : barcode(b), productName(p){}

	//Getters and setters
	string getCode()
	{
		return barcode;
	}

	string getName()
	{
		return productName;
	}

	void setCode(string s)
	{
		barcode = s;
	}

	void setName(string s)
	{
		productName = s;
	}

	//Gets new name and barcode
	void scan()
	{
		string bar, prod;
		cout << "\nEnter barcode: ";
		cin >> bar;
		setCode(bar);
		cout << "Enter product name: ";
		cin >> prod;
		setName(prod);
	}

	//Prints name and barcode
	void print()
	{
		cout << "\nBarcode: " << getCode() << endl;
		cout << "Product name: " << getName() << endl << endl;
	}
protected:
	string barcode, productName;
};

//Prepackaged food child class
class PrepackedFood : public Product
{
public:
	//Default contructor
	PrepackedFood(string b = "barcode", string p = "Default Prepacked", double u = 1) : Product(b,p), unitPrice(u){}
	

	double getUnitPrice()
	{
		return unitPrice;
	}

	void setUnitPrice(double price)
	{
		unitPrice = price;
	}
	
	void scan()
	{
		string bar, prod;
		double price;
		cout << "\nEnter barcode: ";
		cin >> bar;
		setCode(bar);
		cout << "Enter product name: ";
		cin >> prod;
		setName(prod);
		cout << "Enter unit price: ";
		cin >> price;
		setUnitPrice(price);
	}

	void print()
	{
		cout << "\nBarcode: " << getCode() << endl;
		cout << "Product name: " << getName() << endl;
		cout << "Unit price: $" << getUnitPrice() << endl << endl;
	}

private:
	double unitPrice;
};

class FreshFood : public Product
{
public:
	FreshFood(string b = "barcode", string p = "Default Fresh Food", double ppk = 1) : Product(b,p), pricePerKilo(ppk){}
	
	double getPricePerKilo()
	{
		return pricePerKilo;
	}

	void setPricePerKilo(double price)
	{
		pricePerKilo = price;
	}

	void scan()
	{
		string bar, prod;
		double price;
		cout << "\nEnter barcode: ";
		cin >> bar;
		setCode(bar);
		cout << "Enter product name: ";
		cin >> prod;
		setName(prod);
		cout << "Enter price per kilo: ";
		cin >> price;
		setPricePerKilo(price);
	}

	void print()
	{
		cout << "\nBarcode: " << getCode() << endl;
		cout << "Product name: " << getName() << endl;
		cout << "Price per kilo: $" << getPricePerKilo() << "/kg" << endl << endl;
	}

private:
	double pricePerKilo;
};

int main(int argc, char const *argv[])
{
	string code, name;
	double price;
	cout << fixed << setprecision(2);
	Product productOne, productTwo("1234", "Initialised Parent Product");
	
	cout << "Here is the default parent product:\n";
	productOne.print();
	cout << "Update the default parent product:\n";
	productOne.scan();
	cout << "Here is the default parent product after being updated:\n";
	productOne.print();

	cout << "Here is the fully initialised parent product:\n";
	productTwo.print();
	
	PrepackedFood prepackedOne, prepackedTwo("4321", "Initialised Prepacked Food", 5.5);

	cout << "Here is the default prepacked product:\n";
	prepackedOne.print();
	cout << "Update the default prepacked product:\n";
	prepackedOne.scan();
	cout << "Here is the default prepacked product after being updated:\n";
	prepackedOne.print();

	cout << "Here is the fully initialised prepacked product:\n";
	prepackedTwo.print();

	FreshFood freshFoodOne, freshFoodTwo("0987", "Initialised Fresh Food", 3);

	cout << "Here is the default fresh product:\n";
	freshFoodOne.print();
	cout << "Update the default fresh product:\n";
	freshFoodOne.scan();
	cout << "Here is the default fresh product after being updated:\n";
	freshFoodOne.print();

	cout << "Here is the fully initialised fresh product:\n";
	freshFoodTwo.print();

	return 0;
}
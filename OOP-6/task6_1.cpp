#include <iostream>

using namespace std;

class Line
{
public:
	Line(int l) : length(l)
	{
		cout << "Line constructed.\n";
	}
	~Line()
	{
		cout << "Line destroyed.\n";
	}
	void calculate()
	{
		cout << "The line's length is: " << length << endl;
	}
protected:
	int length;
};

class Rectangle : public Line
{
public:
	Rectangle(int l, int w) : Line(l), width(w)
	{
		cout << "Rectangle constructed.\n";
	}
	~Rectangle()
	{
		cout << "Rectangle destroyed.\n";
	}
	void calculate()
	{
		cout << "The rectangle's length is: " << length << endl;
		cout << "The rectangle's area is: " << length * width << endl;
	}
protected:
	int width;
};

class Cuboid : public Rectangle
{
public:
	Cuboid(int l, int w, int h) : Rectangle(l, w), height(h)
	{
		cout << "Cuboid constructed.\n";
	}
	~Cuboid()
	{
		cout << "Cuboid destroyed.\n";
	}
	void calculate()
	{
		cout << "The cuboid's length is: " << length << endl;
		cout << "The cuboid's area is: " << length * width << endl;
		cout << "The cuboid's volume is: " << length * width * height << endl;
	}
protected:
	int height;
};

int main()
{
	int length, width, height;
	cin >> length;
	cin >> width;
	cin >> height;
	Line line(length);
	line.calculate();
	Rectangle rectangle(length,width);
	rectangle.calculate();
	Cuboid cuboid(length,width,height);
	cuboid.calculate();
	return 0;
}
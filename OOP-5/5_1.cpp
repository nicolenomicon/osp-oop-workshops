#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	cout << "=============Part 1===============\n";
	
	int *r;
	int *q;

	r = new int;
	*r = 43;
	q = r;
	//Delete deletes what the pointer points to, which for q is r
	//delete q;
	cout << *r << endl;
	
	cout << "=============Part 2===============\n";
	
	double *p, v;
	p = &v;
	//Sets v to 99.99
	*p = 99.99;
	
	cout << v << endl;
	

	cout << "=============Part 3===============\n";

	int *p1, *p2;
	p1 = new int;
	p2 = new int;
	*p1 = 5;
	*p2 = -1;
	cout << *p1 << " " << *p2 << endl;
	*p1 = *p2;
	cout << *p1 << " " << *p2 << endl;
	*p1 = 20;
	cout << *p1 << " " << *p2 << endl;

	return 0;
}
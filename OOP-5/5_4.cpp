#include <iostream>
#include <string>

using namespace std;

class Student
{
	private:
		string name;
		int numUnits;
		string *unitList;

	public:
		Student(string stuName, int unitNum)
		{
			name = stuName;
			numUnits = unitNum;
			unitList = new string[numUnits];
		}
		~Student()
		{
			delete [] unitList;
		}

		void unitListInput()
		{
			string unit;
			cout << "Enter the " << numUnits << " units enrolled in, line by line" << endl;
			cin.ignore();
			for (int i = 0; i < numUnits; ++i)
			{
				//cin.ignore();
				getline(cin, unit);
				//cin.ignore();
				unitList[i] = unit; 
			}
		}

		void nameUnitDisplay()
		{
			cout << "Student name: " << name << endl;
			cout << "Units: \n";
			for (int i = 0; i < numUnits; ++i)
			{
				
				cout << unitList[i] << endl;
			}
		}
};

int main(int argc, char const *argv[])
{
	string name;
	int units;

	cout << "Enter name: ";
	getline(cin, name);

	cout << "Enter number of units: ";
	cin >> units;
	cout << endl;
	
	Student studentOne(name, units);
	studentOne.unitListInput();
	cout << endl;
	studentOne.nameUnitDisplay();
	
	return 0;
}